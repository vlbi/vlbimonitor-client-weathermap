Create a local store of weathermap images and inform the VLBImonitor of available updates.

Purpose
-------
Serving images directly from the VLBImonitor server reduces download volumes incurred by the web pages in two ways.  First, images only need to be downloaded when they actually change.  Second, images can be compressed to the quality needed for the purpose.

The images store is permanent.  This enables recreating historic states of VLBImonitor webinterface pages.

Design
------
1. download images from different providers in a temporary container
2. copy the result to an "origs" directory if the downloaded version is newer than the one available there
3. make a versioned copy with reduced image quality in the "repo" directory
4. submit the filename of the latest file to the VLBImonitor server.
