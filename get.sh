#!/bin/bash
startdir=$(readlink -f ${0%/*})
inputfile=$startdir/links.txt
authfile=$startdir/auth.txt

#-- origs directory contains files identical to upstream
origs=$startdir/origs
#-- repo directory contains versioned copies in reduced quality
repo=$startdir/repo
#-- packet IDs
packetid=$startdir/packetid
mkdir -p $repo $origs $packetid


submit() {
	site=$1
	data=$2

	SERVER="http://localhost:8000/raw/$site"

	datatime=${data%.*}
	datatime=${datatime#*_}

	handler="curl --silent -o - $SERVER --data @-"
	#handler="cat"

	response="$($handler <<json_end
{
	"weatherMap_waterVapor_url": [[${datatime}, "${data}"]]
}
json_end
	)" || printf "submission error: %s: %s\n" $site "$response"
}


#-- setup a container and start a one of the getter functions
updateInContainer() {
	[[ $# -ne 3 ]] && exit 1
	sites=$1
	fname=$2
	url=$3

	#-- run in container to avoid filename clashes
	tmpdir=/tmp/weatherMaps_${fname%.*}
	mkdir -p $tmpdir
	pushd $tmpdir >/dev/null

	repofile=""
	if [[ "$url" =~ '%%(date15min)' ]]; then
		getsomeMexico $fname $url '%%(date15min)'
	else
		getsingle $fname $url
	fi

	#-- clean up container
	popd >/dev/null
	rm -r $tmpdir

	[[ $repofile == "" ]] && return

	while IFS=',' read -ra sitea; do
		for site in "${sitea[@]}"; do
			submit $site $repofile
		done
	done <<< $sites
}


#-- Each new version has a new file name upstream.  We download them all and
#-- then use the latest.
getsomeMexico() {
	[[ $# -ne 3 ]] && exit 1

	#-- constants
	NDEEP=10

	#-- parse args
	name=${1%.*}
	tgname=$origs/$1
	ending=${1#*.}
	tgname_=$origs/$name
	urll=$2
	srcname=${urll##*/}
	placeholder=$3

	#-- round now down to minutes/15
	now=$(date +%s)
	now_min=$(date -u +%M -d @${now})
	now_offset=$((10#$now_min / 15))
	now_offset=$((now_min - now_offset*15))
	now_round=$(date -u +%Y-%m-%dT%H:%M:00 -d @$((now - now_offset*60)))
	now_round=$(date -u +%s -d ${now_round})

	getone() {
		local datestr=$(date -u +%Y%m%d%H%M -d @$((now_round - 15*60*$i)))

		#-- exists already
		local tgname="${tgname_}_$datestr.$ending"
		if [[ -e $tgname ]]; then
			#echo "exists: $tgname"
			return
		fi

		local url=${urll/"$placeholder"/$datestr}
		wget -U \'\' -N $url --no-verbose --quiet --timeout=20 || return

		local srcname=${url##*/}

		#-- bad download
		file "$srcname" | grep -q 'image data' || {
			echo "error    : ${name}_$datestr"
			ls -l "${srcname}"
			return
		}

		cp -p $srcname $tgname
	}

	for ((i=0; i<=$NDEEP; i++)); do
		getone &
	done
	wait

	#-- clean up origs
	ls ${tgname_}_* | head -n -$((NDEEP)) | xargs rm -vf

	#-- check if a newer latest file exists
	last=$(ls -1 ${tgname_}_* | tail -n 1)
	
	cmp -s $last $tgname
	if [ $? == 0 ]; then
		echo "identical: $name"
		return
	fi

	cp -p --remove-destination $last $tgname
	echo "updated  : $name"

	#-- convert and save versioned copy
	rm -f tmp.jpg
	convert $tgname -quality 60 tmp.jpg
	touch -r $tgname tmp.jpg
	tmod=$(stat -c '%Y' $last)
	repofile=${name}_$tmod.jpg
	cp -pf tmp.jpg $repo/$repofile
}


#-- New versions appear on an identical filename.  We download the current
#-- version, compare it with our latest, and save it if it's different.
getsingle() {
	[[ $# -ne 2 ]] && exit 1

	#-- parse args
	name=${1%.*}
	tgname=$origs/$1
	url=$2
	srcname=${url##*/}

	#-- wget -O and -N are exclusive
	#-- so we copy the previous output file in place
	[[ -e $tgname ]] && cp -pfL $tgname "$srcname"
	wget -N $url --no-verbose --quiet --timeout=20

	#-- bad download
	file "$srcname" | grep -q 'image data' || {
		echo "error    : $name"
		return
	}

	#-- file not new
	cmp -s $tgname "$srcname"
	if [ $? == 0 ]; then
		echo "identical: $name"
		return
	fi

	#-- save result
	cp -p --remove-destination "$srcname" $tgname
	echo "updated  : $name"

	#-- convert and save versioned copy
	rm -f tmp.jpg
	convert "$srcname" -quality 60 tmp.jpg
	touch -r "$srcname" tmp.jpg
	tmod=$(stat -c '%Y' "$srcname")
	repofile=${name}_$tmod.jpg
	cp -pf tmp.jpg $repo/$repofile
}


#-- Start one updater per line in the input file concurrently.
while IFS= read -r line; do
	updateInContainer $line &
done < <(sed -e '/^#/d' $inputfile)
wait
